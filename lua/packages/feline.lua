local modeutils = require 'feline.providers.vi_mode'


local colors = {
	bg = '#1f1d2e',
	bg_alt = '#191724',
	fg = '#e0def4',
	fg_alt = '#26233a',
	love = '#eb6f92',
	gold = '#f6c177',
	rose = '#ebbcba',
	pine = '#31748f',
	foam = '#9ccfd8',
	iris = '#c4a7e7'
}

local vi_mode_colors = {
	NORMAL = colors.rose,
	INSERT = colors.love,
	VISUAL = colors.iris,
	OP = colors.iris,
	BLOCK = colors.iris,
	LINES = colors.iris,
	REPLACE = colors.iris,
	['V-REPLACE'] = colors.iris,
	ENTER = colors.iris,
	MORE = colors.iris,
	SELECT = colors.iris,
	COMMAND = colors.foam,
	SHELL = colors.foam,
	TERM = colors.foam,
	NONE = colors.rose
}

local lsp_get_diag = function(str)
	local count = vim.lsp.diagnostic.get_count(0, str)
	return (count > 0) and ' ' ..  count .. ' ' or ''
end

local function make_separator(fg, bg, type)
	return {
		str = type,
		always_visible = true,
		hl = {
			fg = fg,
			bg = bg,
		},
	}
end


-- Component definitions.
local definitions = {
	vi_mode = {
		provider = function()
			return ' ' .. modeutils.get_vim_mode()
		end,
		right_sep = make_separator(colors.bg, colors.bg_alt, 'slant_right'),
		hl = function()
			local val = {
				name = modeutils.get_mode_highlight_name(),
				fg = modeutils.get_mode_color()
			}
			return val
		end,
	},
	file_info = {
		provider = {
			name = 'file_info',
			opts = {icon = ''}
		},
		left_sep = make_separator(colors.bg_alt, colors.bg_alt, '  '),
		right_sep = 'slant_right_2',
		hl = {
			bg = colors.bg_alt
		},
	},

	lsp_errs = {
		left_sep = make_separator(colors.love, colors.bg, 'left_filled'),
		provider = function()
			return lsp_get_diag('Error')
		end,
		hl = {
			fg = colors.bg_alt,
			bg = colors.love,
		},
	},
	lsp_warn = {
		left_sep = make_separator(colors.gold, colors.love, 'left_filled'),
		provider = function()
			return lsp_get_diag('Warning')
		end,
		hl = {
			fg = colors.bg_alt,
			bg = colors.gold,
		},
	},
	lsp_info = {
		left_sep = make_separator(colors.pine, colors.gold, 'left_filled'),
		provider = function()
			return lsp_get_diag('Information')
		end,
		hl = {
			fg = colors.bg_alt,
			bg = colors.pine,
		},
	},
	lsp_hint = {
		left_sep = make_separator (colors.foam, colors.pine, 'left_filled'),
		provider = function()
			return lsp_get_diag('Hint')
		end,
		hl = {
			fg = colors.bg_alt,
			bg = colors.foam,
		},
	},
	lsp_clients = {
		provider = 'lsp_client_names',
		hl = {
			fg = colors.iris,
		},
	},

	git = {
		provider = 'git_branch',
		icon = {
			str = '  ',
			hl = {
				fg = 'gold',
			},
		},
		left_sep = {
			str = 'slant_left_thin',
			hl = {
				fg = 'text',
			},
		},
	},
	git_add = {},

	file_encoding ={
		provider = 'file_encoding',
		left_sep = {make_separator(colors.bg_alt, colors.foam, 'left_filled'),
			make_separator(colors.bg_alt, colors.bg_alt, ' ')},
		right_sep = make_separator(colors.bg_alt, colors.bg_alt, ' '),
		hl = {
			bg = colors.bg_alt
		}
	},
	file_pos = {
		provider = 'line_percentage',
		right_sep = {make_separator(colors.bg_alt, colors.bg_alt, ' '),
			make_separator(colors.bg_alt, colors.bg, 'slant_right_2'), ' '},
		hl = {
			bg = colors.bg_alt
		},
	},
	line_per = {
		provider = 'position',
		right_sep = ' '
	},
}

-- The actual components in the statusline.
local components = {
	active = {
		{
			definitions.vi_mode,
			definitions.file_info,
		},
		{
			-- definitions.lsp_clients,
		},
		{
			definitions.lsp_errs,
			definitions.lsp_warn,
			definitions.lsp_info,
			definitions.lsp_hint,
			definitions.file_encoding,
			definitions.file_pos,
			definitions.line_per,
		},
	},
	inactive = {
		{},
		{},
		{},
	},
}

-- require'feline'.setup {}
require('feline').setup {
	colors = { bg = colors.bg, fg = colors.fg },
	components = components,
	vi_mode_colors = vi_mode_colors,
	force_inactive = {
		filetypes = {
			'packer',
			'NvimTree',
			'fugitive',
			'fugitiveblame'
		},
		buftypes = {'terminal'},
		bufnames = {}
	}
}
