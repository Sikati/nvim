require('nvim-treesitter.configs').setup {
	highlight = {
		enable = true
	},
	indent = {
		enable = true
	},
	autotag = {
		enable = true
	},
	rainbow = {
		enable = true,
		extended_mode = true,
		max_file_lines = nil,
		colors = {
			"#C792EA",
			"#C3E88D",
			"#82AAFF",
			"#89DDFF",
			"#F78C6C",
			"#FFCB6B",
			"#F07178",
		},
	},
	ensure_installed = {
		-- web development
		"html", "css", "javascript",

		-- scripting
		"bash", "python", "nix",

		-- parsing
		"json", "json5", "yaml",

		-- userspace
		"c_sharp", "lua",

		-- low(ish) level
		"cpp", "rust",
	}
}
