#!/bin/sh
CACHE_PATH="${XDG_CACHE_HOME:-$HOME/.cache/nvim}"
OLD_PATH="${XDG_DATA_HOME:-$HOME/.local/share/nvim/site}"
PACK_PATH="${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/pack/packer/start"
IMPATIENT_PATH="${PACK_PATH}/impatient.nvim"
FILETYPE_NVIM_PATH="${PACK_PATH}/filetype.nvim"
PACKER_PATH="${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/pack/packer/opt/packer.nvim"
LANGUAGE_PATH="${XDG_DATA_HOME:-$HOME/.local/share}/nvim/language-servers"

echo 'Removing old files ...'
rm -rf "$OLD_PATH/lua"
rm -rf "$OLD_PATH/pack"
rm -rf "$CACHE_PATH/luacache"
rm -rf "$CACHE_PATH/log"
rm -rf "$CACHE_PATH/lsp.log"
rm -rf "$CACHE_PATH/packer.nvim.log"

mkdir -p "$PACK_PATH"

echo 'Downloading Packer ...'

if [ ! -d "$PACKER_PATH" ]; then
  git clone "https://github.com/wbthomason/packer.nvim" "$PACKER_PATH"
fi

echo 'Downloading impatient.nvim ...'

if [ ! -d "$IMPATIENT_PATH" ]; then
  git clone "https://github.com/lewis6991/impatient.nvim.git" "$IMPATIENT_PATH"
fi

echo 'Downloading filetype.nvim ...'

if [ ! -d "$FILETYPE_NVIM_PATH" ]; then
  git clone "https://github.com/nathom/filetype.nvim" "$FILETYPE_NVIM_PATH"
fi

if [ ! -e "${CACHE_PATH}/swap" ]; then
  echo "Creating vim swap/backup/undo/view folders inside ${CACHE_PATH}/nvim ..."
  mkdir -p "${CACHE_PATH}/backup"
  mkdir -p "${CACHE_PATH}/session"
  mkdir -p "${CACHE_PATH}/swap"
  mkdir -p "${CACHE_PATH}/tags"
  mkdir -p "${CACHE_PATH}/undo"
  mkdir -p "${CACHE_PATH}/view"
fi

echo 'Sucessfully installed Neovim config'
